const mongoose = require("mongoose");

// const { MONGO_URI } = process.env;

exports.connect = () => {
  // Connecting to the database
  const MONGO_URI="mongodb+srv://doadmin:3Cn2Kp4jcJ80W659@db-mongodb-nyc1-22526-ccc29d80.mongo.ondigitalocean.com/admin?tls=true&authSource=admin"
  mongoose
    .connect(MONGO_URI, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      
    })
    .then(() => {
      console.log("Successfully connected to database");
    })
    .catch((error) => {
      console.log("database connection failed. exiting now...");
      console.error(error);
      process.exit(1);
    });
};
