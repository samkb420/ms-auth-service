require("dotenv").config();
require("../config/database").connect();

const jwt = require('jsonwebtoken');
const User = require("../models/users");

// register a user

exports.register = (req, res) => {

    const { fullname, email, password } = req.body;

    const UserExist = User.findOne({ email });
    UserExist.then(user => {
        if (user) {
            return res.status(400).json({
                message: "User already exists trY Login"
            });
        } else {
            const newUser = new User({
                fullname,
                email,
                password
            });
            newUser.save()
                .then(user => {
                    res.json({
                        message: "User created successfully"
                    });
                }).catch(err => {
                    res.send("Error: " + err);
                }
                );
        }
    }
    );
}

// login a user

exports.login = (req, res) => {
    const { email, password } = req.body;
    const user = User.findOne({ email });
    user.then(user => {
        if (!user) {
            return res.status(400).json({
                message: "User Account not found"
            });
        }
        if (user.password !== password) {
            return res.status(400).json({
                message: "Ooops Incorrect password"
            });
        }
        const token = jwt.sign({
            userId: user._id
        }, process.env.JWT_KEY, {
            expiresIn: "1h"
        });
        res.cookie("token", token, {
            expiresIn: "1h"
        });
        res.json({
            message: "User logged in successfully",
            token,
            user
        });
    }).catch(err => {
        res.send("Error: " + err);
    }
    );
}

// get user profile

exports.getProfile = (req, res) => {
//    get token in authorization header
    const token = req.headers.authorization.split(" ")[1];
    
    if (!token) {
        return res.status(401).json({
            message: "You are not logged in"
        });
    }
    const user = jwt.verify(token, process.env.JWT_KEY);
    const userId = user.userId;
    const userProfile = User.findById(userId);
    userProfile.then(user => {
        if (!user) {
            return res.status(400).json({
                message: "User not found"
            });
        }
        res.json({
            message: "User profile",
            user
        });
    }).catch(err => {
        res.send("Error: " + err);
    }
    );
}








