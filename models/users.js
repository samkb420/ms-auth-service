const mongoose = require("mongoose");

const UserSchema = new mongoose.Schema({

    fullname: {
        type: String,
        required: true
    },
    email: {

        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    last_login: {
        type: Date,
        default: Date.now,
        required: false
    },
    profile_image_url: {
        type: String,
        required: false
    },
    
});


module.exports = mongoose.model("User", UserSchema);

