const express = require('express');
const { login, register, getProfile } = require('../controllers/auth-controller');



const auth = require('../auth/auth');

const router = express.Router();

// login route
router.post('/login', login);

// register
router.post('/register', register);

// logout



// protected routes

// get user profile
router.get('/profile', auth,getProfile);
// get all registers












module.exports = {
    routes:router
}
